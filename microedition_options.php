<?php

// Les blocs Z requis
if (!isset($GLOBALS['z_blocs'])){
	$GLOBALS['z_blocs'] = array('content','head','head_js','header','aside');
}

$plugins = unserialize($GLOBALS['meta']['plugin']);
if(is_array($plugins['IMAGE_RESPONSIVE'])) {
	define("_IMAGE_RESPONSIVE_CALCULER", true);
}
